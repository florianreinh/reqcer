import React from 'react';
import ReactDOM from 'react-dom';
import './semantic/dist/semantic.min.css';
import {Container, Header} from 'semantic-ui-react'

//import moment from 'moment'
import 'moment/locale/de'
import {createStore} from "redux";

import mainReducer from './reducers/main_reducer'
import update from './actions/actions'

const store = createStore(mainReducer);


class DataRows extends React.Component {
    componentDidMount() {
        const url = `http://localhost:3000/data`;

        store.subscribe(() => this.setState(store.getState()));

        fetch(url).then(res => res.text()).then((reducerData) => {
                store.dispatch(update({data: reducerData}))
            });
    }

    render() {
        return <Row data={store.getState()}/>;
    }
}

function Row(props){
    const data = props.data.data;

    return (
        <Container style={{'borderTop': '1px solid', 'marginTop': '10px', 'paddingTop': '10px'}}>
            <Header>
                {/*created.format('lll')*/}
            </Header>
            {/*Data*/}
            <div className={'ui grid'}>{JSON.stringify(data)}</div>
        </Container>
    );
}

class DashBoard extends React.Component {
    render() {
        return (
            <div className="dashboard">
                <DataRows />
            </div>
        );
    }
}

// ========================================

ReactDOM.render(
    <DashBoard />,
    document.getElementById('root')
);
