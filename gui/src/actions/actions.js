export default function update(newData){
    return {type: 'UPDATE', data: newData};
}
