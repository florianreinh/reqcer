const initialState = { data: [] };

function mainReducer(state = initialState, action){
    if (action.type === 'UPDATE') {
        return Object.assign({}, state, {data: state.data.concat(action.data)});
    }

    return state;
}

export default mainReducer;
