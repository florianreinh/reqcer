const {parentPort} = require('worker_threads');
const mongoose = require('mongoose');

const PersonalData = require('../models/personalData');
const Algorithm = require('../models/algorithm');
const ReducedValue = require('../models/reducedValue');

const {PerformanceObserver, performance} = require('perf_hooks');
const obs = new PerformanceObserver((items) => {
    let perfData = items.getEntries().map((item) => {
        return {name: item.name, duration: item.duration}
    });
    parentPort.postMessage({perf_data: JSON.stringify(perfData)});
    performance.clearMarks();
});
obs.observe({entryTypes: ['measure']});

const workerInterval = 2000;

mongoose.connect('mongodb://localhost:27017/queue_db', {useNewUrlParser: true});

function doWork() {
    loadInput().then((input) => reduceData(input))
        .then(() => getLastReducedResult())
        .then((lastReduced) => parentPort.postMessage({info: `Data reduced successfully. Result: ${lastReduced.value}`}))
        .catch((err) => parentPort.postMessage({error: `Error occurred in the reduction worker: ${err}`}));
}

setInterval(doWork, workerInterval);

async function loadInput() {
    performance.mark('LOAD_DATA');
    const data = await PersonalData.find({});
    let algorithm = null;
    performance.mark('END_LOAD_DATA');

    performance.measure('MEASURE PERSDATA LOAD', 'LOAD_DATA', 'END_LOAD_DATA');

    performance.mark('LOAD_ALGOS');
    try {

        const algoIds = await Algorithm.find({}, '_id', {sort: {'_id': 'desc'}});
        algorithm = await Algorithm.findById(algoIds[0]._id);
    } catch (e) {
        throw Error('No algorithm available!');
    }
    performance.mark('END_LOAD_ALGOS');


    performance.measure('MEASURE ALGORITHMS LOAD', 'LOAD_ALGOS', 'END_LOAD_ALGOS');

    return createInputStruct(data, algorithm);
}

function createInputStruct(data, algorithm) {
    return {data: data, algo: algorithm};
}

async function reduceData(input) {
    let algoParsed = eval(input.algo.description_js);

    let result = input.data.reduce((acc, elem) => {
        return algoParsed(acc, elem.orders);
    }, input.algo.neutralElem);

    let reducedValue = new ReducedValue({value: result.toString()});
    reducedValue.save({});
}

async function getLastReducedResult() {
    const reducedValIds = await ReducedValue.find({}, '_id', {sort: {'_id': 'desc'}});

    return ReducedValue.findById(reducedValIds[0]._id);
}
