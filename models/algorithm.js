const mongoose = require('mongoose');

const algorithmSchema = new mongoose.Schema({
    name: String,
    description_js: String,
    neutralElem: String
});

const AlgorithmModel = mongoose.model('Algorithm', algorithmSchema);

module.exports = AlgorithmModel;