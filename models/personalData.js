const mongoose = require('mongoose');

let personalDataSchema = new mongoose.Schema({
        company: String,
        first_name: String,
        last_name: String,
        gender: String,
        zip_code: String,
        city: String,
        country: String,
        orders: Number
});

let personalDataModel = mongoose.model('PersonalData', personalDataSchema);

module.exports = personalDataModel;