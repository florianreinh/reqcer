const mongoose = require('mongoose');

const reducedValueSchema = new mongoose.Schema({
    value: String,
});

const ReducedValueModel = mongoose.model('ReducedValue', reducedValueSchema);

module.exports = ReducedValueModel;