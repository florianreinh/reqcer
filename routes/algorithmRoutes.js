const Algorithm = require("../models/algorithm");
const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

router.put('/', (req, res) => {
    let alg = new Algorithm(req.body);
    alg.save();

    //This works, because mongoose buffers query calls until a connection is available.
    mongoose.connect('mongodb://localhost:27017/queue_db', {useNewUrlParser: true});
    res.end();
});

module.exports = router;