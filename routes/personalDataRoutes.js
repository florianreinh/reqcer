const PersonalData = require("../models/personalData");
const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

/* GET home page. */
router.get('/', (req, res, next) =>  {
    mongoose.connect('mongodb://localhost:27017/queue_db', {useNewUrlParser: true});

    PersonalData.find({}).then( (data) => res.status(200).header("Access-Control-Allow-Origin", "*").header('Content-Type','application/json').send(data)).catch((err) => {
        res.status(500).send(`Ein Fehler ist aufgetreten bei /data. ${err}`);
    })
});

router.put('/', (req, res) => {
    let pdata = new PersonalData(req.body);
    pdata.save();

    //This works, because mongoose buffers query calls until a connection is available.
    mongoose.connect('mongodb://localhost:27017/queue_db', {useNewUrlParser: true});
    res.end();
});

module.exports = router;