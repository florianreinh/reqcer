let  MongoClient = require('mongodb').MongoClient;
let  url = "mongodb://localhost:27017/queue_db";

MongoClient.connect(url, { useNewUrlParser: true }, (err, db) => {
    if (err) throw err;
    db.close().then(() => console.log("Database created!"));
});